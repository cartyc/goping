FROM golang:1.10.0-alpine3.7 as build
WORKDIR /go/src/goping

COPY . .

RUN go build -o goping

# # Prod
FROM alpine
WORKDIR /app
COPY --from=build /go/src/goping /app/
RUN apk add --update ca-certificates
ENTRYPOINT [ "./goping" ]
