package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {

	// Set the ticker interval
	ticker := time.NewTicker(time.Second * 10)

	// Put the ping func in a routine and run it at ticker interval
	go func() {
		for _ = range ticker.C {
			ping()
		}
	}()

	// Sleep for 5 mins and stop
	time.Sleep(time.Minute * 5)
	ticker.Stop()
}

// ping gitlab servers and return time elapsed
func ping() (time.Duration, error) {

	start := time.Now()
	_, err := http.Get("https://gitlab.com")
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	}
	elapsed := time.Since(start)

	fmt.Println(elapsed)
	return elapsed, err
}
